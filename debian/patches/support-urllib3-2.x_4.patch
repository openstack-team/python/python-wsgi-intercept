From 8c6407647514ceced688a89e16125415c87bb463 Mon Sep 17 00:00:00 2001
From: Chris Dent <cdent@anticdent.org>
Date: Wed, 20 Mar 2024 21:47:23 +0000
Subject: [PATCH 4/6] Correct SSL handling for urllib3 based things

It seems that we need to replicate some ssl context
handling done in urllib3 as we are routing around
some of that by using wsgi-intercept, even when
not actively intercepting a host.
---
 wsgi_intercept/__init__.py | 30 +++++++++++++++++++++---------
 wsgi_intercept/_urllib3.py |  2 ++
 2 files changed, 23 insertions(+), 9 deletions(-)

Index: python-wsgi-intercept/wsgi_intercept/__init__.py
===================================================================
--- python-wsgi-intercept.orig/wsgi_intercept/__init__.py
+++ python-wsgi-intercept/wsgi_intercept/__init__.py
@@ -528,14 +528,18 @@ class WSGI_HTTPConnection(HTTPConnection
     """
 
     def __init__(self, *args, **kwargs):
-        print(f"args1 is {args}, kwargs is {kwargs}")
+        """
+        Do a complex dance to deal with urllib3's method signature
+        constraints.
+        """
+        # TODO: This seems really really fragile but is passing
+        # tests.
         if 'host' in kwargs:
             host = kwargs.pop('host')
             if 'port' in kwargs:
                 port = kwargs.pop('port')
             else:
                 port = None
-            print(f"args2 is {args}, kwargs is {kwargs}")
             super().__init__(host, port, *args, **kwargs)
         else:
             if len(args) > 2:
@@ -634,22 +638,31 @@ class WSGI_HTTPSConnection(HTTPSConnecti
                 try:
                     import ssl
                     if hasattr(self, '_context'):
+                        # Extract cert_reqs from requests + urllib3.
+                        # They do some of their own SSL context management
+                        # that wsgi intercept routes around, so we need to
+                        # be careful.
+                        if hasattr(self, '_intercept_cert_reqs'):
+                            cert_reqs = self._intercept_cert_reqs
+                        else:
+                            cert_reqs = self.cert_reqs
+
                         self._context.check_hostname = self.assert_hostname
                         self._check_hostname = self.assert_hostname     # Py3.6
                         if hasattr(ssl, 'VerifyMode'):
                             # Support for Python3.6 and higher
-                            if isinstance(self.cert_reqs, ssl.VerifyMode):
-                                self._context.verify_mode = self.cert_reqs
+                            if isinstance(cert_reqs, ssl.VerifyMode):
+                                self._context.verify_mode = cert_reqs
                             else:
                                 self._context.verify_mode = ssl.VerifyMode[
-                                    self.cert_reqs]
-                        elif isinstance(self.cert_reqs, str):
+                                    cert_reqs]
+                        elif isinstance(cert_reqs, str):
                             # Support for Python3.5 and below
                             self._context.verify_mode = getattr(ssl,
-                                    self.cert_reqs,
+                                    cert_reqs,
                                     self._context.verify_mode)
                         else:
-                            self._context.verify_mode = self.cert_reqs
+                            self._context.verify_mode = cert_reqs
 
                     if not hasattr(self, 'key_file'):
                         self.key_file = None
@@ -668,7 +681,6 @@ class WSGI_HTTPSConnection(HTTPSConnecti
                             else:
                                 self._check_hostname = self.check_hostname
                 except (ImportError, AttributeError):
-                    import traceback
                     traceback.print_exc()
                 HTTPSConnection.connect(self)
 
Index: python-wsgi-intercept/wsgi_intercept/_urllib3.py
===================================================================
--- python-wsgi-intercept.orig/wsgi_intercept/_urllib3.py
+++ python-wsgi-intercept/wsgi_intercept/_urllib3.py
@@ -43,6 +43,8 @@ def make_urllib3_override(HTTPConnection
 
         def __init__(self, *args, **kwargs):
             print(f"{args}:::{kwargs}")
+            if 'cert_reqs' in kwargs and kwargs['cert_reqs'] is not None:
+                self._intercept_cert_reqs = kwargs.pop("cert_reqs")
             for kw in HTTPS_KEYWORD_POPS:
                 kwargs.pop(kw, None)
             if sys.version_info > (3, 12):
